/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package readability.Readability;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import readability.WordCounter;
import readability.WordCounterGUI;

/**
 * Main class to execute the program.
 * @author Supanat Pokturng
 * @version 2015.04.07
 *
 */
public class Main {
	
	/**
	 * Execute the program.
	 * @param args is an arguments
	 */
	public static void main(String[] args) {
		if( args.length != 0 ) {
			WordCounter wc = new WordCounter();
			URL url = null;
			InputStream in = null;
			String file_dir = args[0];
			wc.setLink(file_dir);
			if(file_dir.contains("http://")) {
				try {
					url = new URL(file_dir);
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}
				try {
					in = url.openStream();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			else {
				File file = new File(file_dir);
				in = null;
				try {
					in = new FileInputStream(file);
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				}
			}
			BufferedReader breader = new BufferedReader( new InputStreamReader(in) );
			while( true ) {
				String line = "";
				try {
					line = breader.readLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
				if (line == null) break;
				if( line == "") wc.setTotalSentences(wc.getTotalSentences()+1);
				String newName = line.replaceAll("[\",()/]","");
				String [] arr = newName.split(" ");
				for( String word : arr)
					wc.setTotalSyllables(wc.getTotalSyllables()+ wc.count(word));
			}
			wc.setFleschIndex();
			System.out.println(wc.result());
		}
		else{
			/* Declare and run the GUI of the program */
			WordCounterGUI gui = new WordCounterGUI();
		}
		
	}
}
