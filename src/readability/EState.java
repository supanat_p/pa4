/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package readability;

/**
 * State for the character "E".
 * @author Supanat Pokturng
 * @version 2015.04.07
 */
public class EState implements State {
	
	/**
	 * Check the character for the next state.
	 * @param c is a character that want to check.
	 * @param wordCounter is word counter that we keep the
	 * count of syllables, sentences and words.
	 */
	public void handleChar(char c , WordCounter wordCounter) {
		/* Go to vowel state when the character is vowel */
		if( isVowel(c) )
			wordCounter.setState(wordCounter.getVowelState());
		/* Go to consonant state when the
		 * character is consonant */
		else if( Character.isLetter(c) )
			wordCounter.setState(wordCounter.getConsonantState());
		/* Go to dash state when found '-' */
		else if( c=='-' )
			wordCounter.setState(wordCounter.getDashState());
		/* Count the sentence when found 
		 * the end sentence symbol */
		else if( isEndSentence(c)) {
			wordCounter.setTotalSentences(wordCounter.getTotalSentences()+1);
		}
		/* Go to non-word state when the character is the others symbols */
		else
			wordCounter.setState(wordCounter.getNonWordState());
	}

}
