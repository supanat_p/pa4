/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package readability;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Word counter can count the syllables, words and sentences.
 * Can calculate readability of this file.
 * @author Supanat Pokturng
 * @version 2015.04.07
 */
public class WordCounter {
	
	/* Initialize and declare */
	State state;
	private int count = 0;
	protected int totalSyllables = 0;
	protected int totalSentences = 0;
	protected int totalWords = 0;
	protected State startState = new StartState();
	protected State vowelState = new VowelState();  
	protected State eState = new EState();  
	protected State nonWordState = new NonWordState();  
	protected State consonantState = new ConsonantState();  
	protected State dashState = new DashState(); 
	private double fleschIndex;
	private URL url;
	private InputStream in;
	private String link;
	
	/**
	 * Count the syllables, words and sentences
	 * of the text by using state.
	 * @param word is the text that want to count
	 * @return number of the syllables
	 */
	public int count( String word ) {
		count = 0;
		state = startState;
		/* Check every character in each word */
		for( char c : word.toCharArray() ) {
			state.handleChar(c , this);
		}
		/* If the last character is 'e' and syllables not one,
		 * decrease the syllable by one.*/
		if( state == eState && count != 1) {
			count--;
			totalWords++;
		}
		/* Count the word */
		else if( count != 0) {
			totalWords++;
		}
		/* If it's not the word, don't count word. */
		else if( state == nonWordState) {
			totalWords--;
		}
		return count;
	}
	
	/**
	* Count all words read from a text in url or 
	* file directory and return the total count.
	* @param link is the String of file directory or url of file
	* @return number of words in the URL.
	* @throws IOException 
	*/
	public int countWords(String link) throws IOException {
		
		this.link = link;
		int count = 0;
		/* If link is url, set InputStream to url that open stream. */
		if(link.contains("http://")) {
			url = new URL(link);
			in = url.openStream( );
		}
		/* If link is not url, it's file directory.
		 * Set input stream to file */
		else {
			File file = new File(link);
			in = null;
			try {
				in = new FileInputStream(file);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
		}
		
		BufferedReader breader = new BufferedReader( new InputStreamReader(in) );
		while( true ) {
			/* Get String of each line */
			String line = breader.readLine();
			if (line == null) break;
			/* Count the sentence */
			if( line == "") totalSentences++;
			/* Split each word to array */
			String newName = line.replaceAll("[\",()/]","");
			String [] arr = newName.split(" ");
			/* Count the syllables in array */
			for( String word : arr)
				totalSyllables += count(word);
		}
		/* calculaate the flesch index */
		this.setFleschIndex();
		return count;
	}
	
	/**
	 * Check this character is the vowel or not.
	 * @param c is a character that want to check
	 * @return true if it's a vowel
	 */
	public boolean isVowel(char c) {
		String vowelList = "AEIOUaeiou";
		String temp = c+"";
		return vowelList.contains(temp);
	}
	
	/**
	 * Set a new state to word counter.
	 * @param newState is the new state that want to change
	 */
	public void setState( State newState ) {
		this.state = newState;
	}
	
	/**
	 * Get the consonant state.
	 * @return consonant state
	 */
	public State getConsonantState() {
		return this.consonantState;
	}
	
	/**
	 * Get the vowel state.
	 * @return state of vowel
	 */
	public State getVowelState() {
		return this.vowelState;
	}
	
	/**
	 * Get the dash state.
	 * @return state of dash
	 */
	public State getDashState() {
		return this.dashState;
	}
	
	/**
	 * Get the e state.
	 * @return state of E
	 */
	public State getEState() {
		return this.eState;
	}
	
	/**
	 * Get the non-word state.
	 * @return state of non-word
	 */
	public State getNonWordState() {
		return this.nonWordState;
	}
	
	/**
	 * Get count of the word counter.
	 * @return count
	 */
	public int getCount() {
		return this.count;
	}
	
	/**
	 * Set a new count of word counter.
	 * @param newCount is a new count that want to set
	 */
	public void setCount( int newCount ) {
		this.count = newCount;
	}
	
	/**
	 * Get total syllables of text.
	 * @return total of syllables
	 */
	public int getTotalSyllables() {
		return this.totalSyllables;
	}
	
	/**
	 * Set a new total sentences of text.
	 * @param newTotalSentences is new total sentences
	 */
	public void setTotalSentences( int newTotalSentences ) {
		this.totalSentences = newTotalSentences;
	}
	
	/**
	 * Get a total sentences of word counter.
	 * @return number of sentences
	 */
	public int getTotalSentences() {
		return this.totalSentences;
	}
	
	/**
	 * Get total word of word counter.
	 * @return number of total words
	 */
	public int getTotalWords() {
		return this.totalWords;
	}
	
	/**
	 * Calculate the flesch index of text.
	 * @return flesch index of text
	 */
	public double calFleschIndex() {
		return 206.835 - ( 84.6*( this.getTotalSyllables() * 1.0 / this.getTotalWords() ) ) 
				- (1.015*( this.getTotalWords() * 1.0 / this.getTotalSentences() ) );
	}
	
	/**
	 * Calculate the flesch index.
	 */
	public void setFleschIndex() {
		this.fleschIndex = calFleschIndex();
	}
	
	/**
	 * Get flesch index.
	 * @return flesch index
	 */
	public double getFleschIndex() {
		return this.fleschIndex;
	}
	
	/**
	 * Transfer to reability.
	 * @return String of readability
	 */
	public String getReadability() {
		if( this.getFleschIndex() < 0 )
			return "Advanced degree graduate";
		else if( this.getFleschIndex() <= 30 )
			return "College graduate";
		else if( this.getFleschIndex() <= 50 )
			return "College student";
		else if( this.getFleschIndex() <= 60 )
			return "High school student";
		else if( this.getFleschIndex() <= 65 )
			return "9th grade student";
		else if( this.getFleschIndex() <= 70 )
			return "8th grade student";
		else if( this.getFleschIndex() <= 80 )
			return "7th grade student";
		else if( this.getFleschIndex() <= 90 )
			return "6th grade student";
		else if( this.getFleschIndex() <= 100 )
			return "5th grade student";
		else
			return "4th grade student (elementary school)";
	}
	
	/**
	 * Set total syllables of text.
	 * @param newTotalSyllables is new total syllables
	 */
	public void setTotalSyllables( int newTotalSyllables ) {
		this.totalSyllables = newTotalSyllables;
	}
	
	/**
	 * Set a new link to word counter.
	 * @param newLink is new link to set
	 */
	public void setLink( String newLink ) {
		this.link = newLink;
	}
	
	/**
	 * Get a result of word counter. 
	 * @return String of result
	 */
	public String result() {
		String directory = this.link;
		double fleschIndex = getFleschIndex();
		String readAbility = getReadability();
		int sentences = getTotalSentences();
		int words = getTotalWords();
		int syllables = getTotalSyllables();
		String format = " Directory : %s\n\n Syllable : %d\n Sentence : %d\n Word : %d\n Flesch Index : %.2f\n Readability : %s";
		String description = String.format(format, directory , syllables , sentences , words , fleschIndex , readAbility );
		return description;
	}
}

