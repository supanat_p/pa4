/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package readability;
/**
 * Interface of the states.
 * @author Supanat Pokturng
 * @version 2015.03.26
 */
public interface State {
	
	/**
	 * Check this character have to go to what state.
	 * @param c is a character that want to check
	 * @param wordCounter is word counter that we keep the
	 * count of syllables, sentences and words.
	 */
	public void handleChar( char c , WordCounter wordCounter);
	
	/**
	 * Check that character is a vowel(a,e,i,o,u) or not.
	 * @param c is a character that want to check
	 * @return true if this character is vowel
	 */
	default boolean isVowel(char c){
		String vowelList = "AEIOUaeiou";
		String temp = c+"";
		return vowelList.contains(temp);
	}
	
	/**
	 * Check that character is a symbol that mean
	 * ending sentence( . , ! , ? , ; ) or not.
	 * @param c is a character that want to check
	 * @return true if this character is a symbol
	 * that end sentence.
	 */
	default boolean isEndSentence( char c ) {
		String signalList = ".!?;";
		String temp = c+"";
		return signalList.contains(temp);
	}
}
