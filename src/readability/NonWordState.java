/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package readability;

/**
 * State for the non-word character.
 * @author Supanat Pokturng
 * @version 2015.04.07
 */
public class NonWordState implements State {

	/**
	 * Check the character for the next state.
	 * @param c is a character that want to check.
	 * @param wordCounter is word counter that we keep the
	 * count of syllables, sentences and words.
	 */
	public void handleChar( char c , WordCounter wordCounter ) {
		/* Always set count of syllables to zero
		 * because it's not a word. */
		wordCounter.setCount(0);
	}

}
