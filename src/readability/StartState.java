/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package readability;

/**
 * State for the first character of each word.
 * @author Supanat Pokturng
 * @version 2015.04.07
 */
public class StartState implements State {
	
	/**
	 * Check the character for the next state.
	 * @param c is a character that want to check.
	 * @param wordCounter is word counter that we keep the
	 * count of syllables, sentences and words.
	 */
	public void handleChar(char c, WordCounter wordCounter) {
		
		/* Go to E state and count the syllable when 
		 * the character is 'e' */
		if( c=='e' || c=='E') {
			wordCounter.setCount(wordCounter.getCount()+1);
			wordCounter.setState(wordCounter.getEState());
		}
		/* Go to vowel state and count the syllable when
		 * the character is a vowel or 'y' */
		else if( isVowel(c) || c=='y' || c=='Y' ) {
			wordCounter.setCount(wordCounter.getCount()+1);
			wordCounter.setState(wordCounter.getVowelState());
		}
		/* Go to consonant state when the
		 * character is consonant */
		else if( Character.isLetter(c) )
			wordCounter.setState(wordCounter.getConsonantState());
		/* Go to non-word state when the character is the others symbols */
		else
			wordCounter.setState(wordCounter.getNonWordState());
	}

}
