package readability;
import java.io.File;

import javax.swing.filechooser.FileFilter;

public class TextFileFilter extends FileFilter {
		 /** accept(File f) method required by the interface */
		public boolean accept(File f) {
			if ( f.isDirectory() ) return true;
			if ( ! f.isFile() ) return false;
			String ext = getExtension(f);
			if ( ext.equals("txt") ) return true;
/* to include HTML files also add these extensions
			if ( ext.equals("htm") ) return true;
			if ( ext.equals("html") ) return true;
 */
			// anything else return false
			return false;
		}
		
		/**
		 * Describe this file filter.  
		 * Will be displayed in dialog. *
		 * @return String of description
		 */
		public String getDescription() {
			return "Text (.txt) Files";
		}
		
		/** utility method used by accept()
		 *  it returns file extension of the parameter's filename
		 */
		String getExtension(File f) {
			String filename = f.getName();
			int k = filename.lastIndexOf('.');
			if ( k <= 0 || k >= filename.length() -1 ) return "";
			else return filename.substring(k+1).toLowerCase();
		}
	} 