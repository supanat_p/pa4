/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package readability;

/**
 * State for the dash character.
 * @author Supanat Pokturng
 * @version 2015.04.07
 */
public class DashState implements State {

	/**
	 * Check the character for the next state.
	 * @param c is a character that want to check.
	 * @param wordCounter is word counter that we keep the
	 * count of syllables, sentences and words.
	 */
	public void handleChar( char c , WordCounter wordCounter) {
		/* Go to vowel state when found the vowel
		 * and count the syllable */
		if ( isVowel(c) ) {
			wordCounter.setCount(wordCounter.getCount()+1);
			wordCounter.setState(wordCounter.getVowelState());
		}
		/* Go to consonant state when found
		 * the consonant */
		else if ( Character.isLetter( c ) )
			wordCounter.setState(wordCounter.getConsonantState());
		/* Go to non-word state when found the symbol */
		else
			wordCounter.setState(wordCounter.getNonWordState());
	}

}
