/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package readability;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;

/**
 * Graphical user interface of word counter program.
 * Show the result of the text in the file
 * That is number of syllables, words and sentences
 * @author Supanat Pokturng
 * @version 2015.04.07
 */
public class WordCounterGUI extends JFrame {
	
	/* Declare the variables */
	private JLabel lb_name;
	private JTextField txt_name;
	private JButton btn_browse , btn_count , btn_clear;
	private JTextArea txtArea_description;
	private JFileChooser fc;
	private WordCounter wordCounter;
	
	/**
	 * Constructor of the GUI.
	 */
	public WordCounterGUI() {
		initComponents();
	}
	
	/**
	 * Initialize the structure of the interface.
	 */
	public void initComponents() {
		
		setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE);
		setTitle( "Readability by Supanat Pokturng");
		
		/* Initialize the components of the interface */
		lb_name = new JLabel("File or URL name : ");
		txt_name = new JTextField(30);
		btn_browse = new JButton( "Browse.." );
		btn_count = new JButton( "Count" );
		btn_clear = new JButton( "Clear" );
		txtArea_description = new JTextArea( 15 , 30 );
		txtArea_description.setEditable(false);
		txtArea_description.setBackground(Color.WHITE);
		
		/* Add the action to this button. */
		btn_browse.addActionListener( new ActionListener() {
			
			/**
			 * Choose the file that want to count 
			 * by using JFileChooser.
			 */
			public void actionPerformed(ActionEvent e) {
				fc = new JFileChooser();
				/* Add a text file filter to
				 * the file chooser */
				FileFilter filter = new TextFileFilter();
				fc.addChoosableFileFilter( filter );
				fc.setCurrentDirectory( new File("C:/") );
				fc.showOpenDialog(WordCounterGUI.this);
				File fileStr = fc.getSelectedFile();
				/* If select file, show directory in text field */
				if(fileStr != null)
					txt_name.setText(fileStr.getAbsolutePath());
			}
		});
		
		/* Add action to this button */
		btn_count.addActionListener( new ActionListener() {
			
			/**
			 * Count the syllables, words, and sentences in
			 * the file in text field and show result.
			 */
			public void actionPerformed(ActionEvent e) {
				/* Initialize the word counter */
				wordCounter = new WordCounter();
				try {
					/* Start to count in word counter */
					wordCounter.countWords(txt_name.getText());
				} catch (IOException e1) {
					e1.printStackTrace();
				}	
					txtArea_description.setText(wordCounter.result());
			}
		});
		
		/* Clear text in text field and text area. */
		btn_clear.addActionListener( new ActionListener() {
			
			/**
			 * Set text field to blank.
			 */
			public void actionPerformed(ActionEvent e) {
				txtArea_description.setText("");
				txt_name.setText("");
			}
		});
		/* Arrange the components in the interface. */
		Panel pane = new Panel();
		pane.setLayout( new BoxLayout( pane , BoxLayout.Y_AXIS ));
		Container container = super.getContentPane();
		Panel paneTop = new Panel();
		paneTop.setLayout( new FlowLayout() );
		paneTop.add(lb_name);
		paneTop.add(txt_name);
		paneTop.add(btn_browse);
		
		Panel paneMid = new Panel();
		paneMid.setLayout( new FlowLayout() );
		paneMid.add(btn_count);
		paneMid.add(btn_clear);
		
		pane.add(paneTop);
		pane.add(paneMid);
		pane.add(txtArea_description);
		
		container.add(pane);
		pack();
		super.setVisible(true);	
	}
}