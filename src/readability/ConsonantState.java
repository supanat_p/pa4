/**
 * This source code is Copyright 2015 by Supanat Pokturng.
 */
package readability;

/**
 * State for the consonant character.
 * @author Supanat Pokturng
 * @version 2015.04.07
 */
public class ConsonantState implements State {
	
	/**
	 * Check the character for the next state.
	 * @param c is a character that want to check.
	 * @param wordCounter is word counter that we keep the
	 * count of syllables, sentences and words.
	 */
	public void handleChar( char c , WordCounter wordCounter) {
		/* Go to E state and count the syllable
		 * when the character is 'e' */
		if( c=='e' || c=='E'){
			wordCounter.setCount(wordCounter.getCount()+1);
			wordCounter.setState(wordCounter.getEState());
		}
		/* Go to vowel state and count the syllable 
		 * when the character is vowel or 'y' */
		else if( isVowel(c) || c=='y' || c=='Y'){
			wordCounter.setCount(wordCounter.getCount()+1);
			wordCounter.setState(wordCounter.getVowelState());
		}
		/* Go to consonant state when the
		 * character is not a vowel*/
		else if( Character.isLetter(c) )
			wordCounter.setState(wordCounter.getConsonantState());
		/* Go to dash state when the character
		 * is '-'*/
		else if( c=='-' )
			wordCounter.setState(wordCounter.getDashState());
		/* Go to consonant state when found apostrophe */
		else if( c=='\'')
			wordCounter.setState(wordCounter.getConsonantState());
		/* Count the sentence when found the symbol that end sentence */
		else if( isEndSentence(c)) {
			wordCounter.setTotalSentences(wordCounter.getTotalSentences()+1);
		}
		/* Go to non-word state when found the other symbols. */
		else
			wordCounter.setState(wordCounter.getNonWordState());
	}

}
